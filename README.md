# MoDaK - Microservices on Docker and Kubernetes

## some info about mkdocs

### extensions needed

- admonition
- markdown_include.include:
      base_path: docs

### gitlab-ci.yaml

- Job name has to be **pages**, so gitlab properly finds it.
- Also artifacts path has to be called **public**.