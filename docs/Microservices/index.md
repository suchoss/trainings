# Co jsou mikroservisy?

## Ideální mikroservisa

- Mikroservisa by ideálně neměla být větší, než co se nám vejde do hlavy. Měli bychom být schopni chápat veškerou logiku v mikroservise obsaženou.
- Jednovláknová.
- Skrývá implementační detaily - Měla by mít interface a měli bychom být schopni změnit implementaci, aniž by si toho uživatel všiml.
- Nezávislá na platformě. Neměla by využívat specifické funkce a chování platformy (např. windows registry).
- Nesdílí přístup k datům! Servisy by si navzájem neměly měnit data.

### Zabezpečená

- Začněte psát zabezpečené servisy - zabezpečenou komunikaci, autentizaci, autorizaci. Nemusí to být perfektní, ale je potřeba s tím začít.
- JWT dává smysl.
- Veškerá komunikace by měla být šifrovaná.

### Autonomní

- ! Nezávisle deployovatelná. Neměla by být závislá na deploymentu čehokoliv jiného, aby mohla začít fungovat.
- Decentralizovaná. Funguje samostatně.

### Spolehlivá

- Logování a monitoring jsou nutné.
- Dostupná celou dobu.
- Inforumuje nás, pokud selže.
- Měl by být implementován tzv. "hearbeat".
- Idempotentní - musí být možné poslat stejný request mikroservise několikrát a pokaždé dostat stejnou odpověď. Stav se může změnit pouze při prvním requestu.

### Odolná vůči chybám

- Měla by být schopna se dostat z chybového stavu.
- Ideálně nesvázaná jen s jedním datacentrem.

### Škálovatelná

- Mikroservisa jako taková by měla být bezstavová.
- Mělo by být možné spustit několik instancí mikroservisy na několika různých strojích.

### Transakční

- Každé volání mikroservisy by mělo být jako transakce
- Datová konzistence tak jak jí známe z relačních databází je iluze - je dosažitelná pouze v rámci jedné mikroservisy. Ve světě mikroservis se bavíme o datové konzistenci ["eventuální"](https://en.wikipedia.org/wiki/Eventual_consistency)

## Komunikace

- Použití REST pro komunikaci mezi servisama není nejlepší nápad.
- V praxi je lepší mít Api Gateway službu (např. [Krakend](https://www.krakend.io/features/)), která překládá rest na messaging.
  
- Každé volání by mělo obsahovat následující atributy:
    - verze (Major.Minor.Patch - 1.0.2, abychom věděli, která verze mikroservisy bude volána)
    - requestid (např. GUID, abychom nezpracovali stejnou zprávu vícekrát)
    - correlationid (např. GUID, abychom dokázali zjistit, kterýma servisama zpráva putovala)
    - authToken (např. JWT, abychom mohli autorizovat požadavek)
  
- Mikroservisy komunikují skrze messaging:
    - Zprávy by měli být na platformě/prog. jazyku nezávislé. (JSON, protobuf)
    - Nemělo by záležet na pořadí zpráv.
    - Převážná většina komunikace mezi mikroservisami by měla být asynchronní - pub/sub pattern.
    - Request/response pattern by měl být použit nanejvýše když voláme cizí služby, nebo brány, které tyto služby volají.
  
- Má smysl vytvořit gateway službu pro komunikaci s okolním světem (ale pouze za předpokladu, že ji bude využívat více servis)
- O mikroservise přemýšlejte jako o objektu - přemýšlejte o tom, co pro vás služba může udělat. Nikdy nepřemýšlejte o službě, jaká data vám může dát.
- Healthcheck, swarming, reporting a deployment za vás udělá Kubernetes.
- ! Nesdílejte knihovny a (business) kód mezi službami. * Výjimkou může být rozhraní (knihovny) pro komunikaci a správu mikroservisy.
- Za žádnou cenu nedávejte business logiku na frontend.

## Reporting

- Pro reporting je ideální využít nějaký datový sklad, kde se schází data. Nesnažte se za každou cenu skládat reporty nad mikroservisami.
- Jsou dvě možnosti, jak správně získávat data z mikroservis:
    - Brát data přímo z databáze pumpou. [Debezium](https://debezium.io/)
    - Brát data z API mikroservisy.

## Testování mikroservis

- Každá mikroservisa by měla také obsahovat také testy. Ideálně bychom měli při psaní mikroservis používat BDD.
- Dobré je mít také testy, které zkusí servisu schodit a pozorují, jak se při tom chová.
- Velký význam má testování celku na frontendu.

### BDD

- Začneme psát testy spolu s interfacem mikroservisy.
- Díky návrhu interface si následně ušetříme dost práce tím, že nebudeme psát zbytečné implementace.
- Unit/Behaviour testy spolu s interface jsou nejlepší možnou programátorskou dokumentací.
- Při BDD testujeme pouze chování aplikace jako celku, nikoliv konkrétní implementaci, která k výsledku vede.

## DDD - Domain Driven Design

- Při návrhu architektury a jednotlivých mikroservis bychom se měli soustředit na DDD.
- Při přemýšlení nad problémem řešíme vždy pouze problémy dané domény.
- Neměli bychom uvažovat nad tím, jak funguje současný systém. Návrh by měl být, jako kdybychom neměli žádný systém.
- Entity se stejným názvem v rozdílných doménách jsou odlišné věci. Nepoužívejme pro ně stejný kód. (Není kniha jako kniha, objednávka x objednávka, autor x autor)
- [Event storming](https://www.eventstorming.com/book)

Minimální architektura
![VM](./images/basic_architecture.png)

Jak může vypadat výsledná architektura
![VM](./images/full_architecture.png)

