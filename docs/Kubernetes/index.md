# Co je Kubernetes?

Kubernetes (k8s) je clusterová technologie, která umožňuje spojit dohromady několik oddělených počítačů. Tyto spojené počítače se poté tváří jako jeden počítač (cluster).

## Cluster

K8s cluster je virtuální propojení několika počítačů dohromady. Tyto počítače se poté ovládají jako jeden stroj a při "instalaci" aplikací (vytváření [podů](#pods)) k8s určuje, na který fyzický počítač pod umístí.

![K8s cluster](./images/cluster.png)

## Nodes

Nody jsou jednotlivé počítače (servery), ze kterých se cluster skládá. Počítače nemusí být umístěny pouze v jednom datacentru. Kubernetes se postará, aby spolu navzájem komunikovali.

![K8s cluster](./images/nodes.png)

## Pods

Pod je instancí aplikace, kterou chceme provozovat. K8s určí, na který fyzický počítač v clusteru pod umístí.

Pod je také nejmenší jednotkou, která jde deployovat. Jeden pod může obsahovat několik [kontejnerů](#containers). Obecně se však používá deployment, kde jeden pod obsahuje jeden kontejner[^1].

![K8s cluster](./images/pods.png)

[^1]: Výjímkou v tomto případě mohou být tzv. [sidecars](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/).

## Containers

[Kontejner](../Docker/index.md#container) je balíček, který obsahuje OS, potřebné knihovny a aplikaci, kterou chcete provozovat. Zjednodušeně lze říci, že kontejner je aplikace.

![K8s cluster](./images/containers2.png)

## ReplicaSets

ReplicaSet určuje kolik instancí (replik) konkrétního podu poběží v danou chvíli. K8s pak zajišťuje, aby běžící počet instancí podu odpovídal definici v ReplicaSetu.

> Pokud vytvoříme ReplicaSet se 3 replikami podu A, K8s se postará o to, aby běželi právě 3.

ReplicaSet hlídá pouze počty replik. Nehlídá však, zda-li se změnila definice podu. Z toho důvodu se doporučuje používat [deploymenty](#deployments).

Obecně životnost podu v K8s clusteru je velmi malá a je zapotřebí zajistit, abychom měli k dispozici vždy nějaký pod.

> Například pokud pod přestane komunikovat (neodpovídá na healthchecky) je smazán a ReplicaSet zajistí vytvoření nového podu.

## Deployments

Deployment je souborem požadavků. Říká, jak má vypadat pod, jak se má chovat a kolik jeho instancí má být v provozu. K8s poté zajišťuje, aby byly všechny podmínky splněny.

Změní-li definice deploymentu, K8s se postará o to aby běžící pody odpovídali definici.

> Při změně definice podu kubernetes vytvoří nové pody a poté staré smaže.

![K8s cluster](./images/deployment.png)

## DaemonSets

Daemonset je podobný deploymentu s jedním rozdílem - neurčuje se počet replik. DaemonSet zajišťuje, aby se na každém nodu spustila jedna replika podu.

## Services

Services slouží k zajištění komunikace mezi pody v clusteru a také mezi pody a vnějším světem.

Pody samy o sobě mají krátkou životnost. Každý pod má přidělenu vlastní (interní) IP adresu a tyto adresy se s životností podu mění.

> Není zaručeno, že při restartování (zabití podu a vytvoření nového) podu, zůstane zachována stejná IP adresa.

Také v případě více replik nějakého podu potřebujeme vyřešit, se kterým konkrétním podem budeme komunikovat.

!!! hint
    Příkladem může být webová služba, která zpracovává textové soubory. Pro zajištění dobré dostupnosti jsou spuštěny 4 repliky této služby. Všechny repliky dělají stejnou práci a tak jsou navzájem zastupitelné - klientovi je jedno, která replika text zpracuje, ale chce mít jen jeden odkaz na službu. Proto je důležité vytvořit Service, která dokáže požadavky rozesílat mezi jednotlivé pody.

Service je trvalým odkazem na pody.

![K8s cluster](./images/services.png)
  
## Ingress

Chceme-li vystavit nějakou službu (service) do internetu, máme několik možností:
- Nastavíme Service type jako **[NodePort](https://kubernetes.io/docs/concepts/services-networking/service/#nodeport)**  
Kubernetes službě přiřadí port na každém nodu (default: 30000-32767). Služba poté bude dostupná na všech nodech.
- Nastavíme Service type jako **[LoadBalancer](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)**  
Tato funkčnost závisí na konkrétní instalaci kubernetes.
- Použijeme ingress  
V ingressu se definují pravidla routování komunikace.

Specifikace ingressu určí, jak bude ingress controller routovat komunikaci.

### Ingress controller

Ingress controller bývá ve skutečnosti pod, ve kterém běží container s reverse proxy/load balancerem. Běžně se používá [nginx community](https://github.com/kubernetes/ingress-nginx), [nginx original](https://github.com/nginxinc/kubernetes-ingress), případně [traefik](https://traefik.io/).

![ingress](./images/ingress.png)

## Persistent volumes

Protože většina podů má krátkou životnost, tak data uložena v podech jsou smazána spolu s podem. Abychom zachovali data, po delší dobu, než je životnost podu, musíme k podu (do kontejneru) připojit Persitstent volume (PV). Persistent volumes jsou rezervací místa na disku.

!!! hint
    Příkladem může být pod, ve kterém je provozována PostgreSQL.  
      
    Pokud budeme provozovat jednu instanci podu a budeme chtít aktualizovat kontejner v podu na novou verzi (budeme chtít aktualizovat PostgreSQL z verze 10.7 na verzi 11.2) pomocí změny v deploymentu tak:  
    - Při nepřipojeném PVC se nám během aktualizace smaže starý pod a spustí nový pod. Přijdeme o všechna data uložená v databázi.  
    - Při připojeném PVC se nám během aktualizace smaže starý pod a spustí nový pod, ale oba pody (nový i starý) budou mít svá data uložena v PV a odkaz (PVC) na data zůstane zachovaný.

**Persistent volume** - PV je rezervace nějakého místa na disku. Jedná o vytvoření zdroje pro Kubernetes.

**Persistent volume claim** - PVC je oprávnění k použití PV. Když potřebujeme přidělit nějaký prostor containeru, požuijeme PV.

V praxi je jednodušší vytvářet pouze PVC a nechat si k nim přidělovat PV dynamicky.

## StatefulSets

StatefulSets jsou podobné deploymentu. Narozdíl od deploymentu jsou však určeny pro aplikace (pody), které si udržují stav. Každý pod má tak svoji unikátní identitu. Každý pod také dostane přidělen svůj vlastní PVC.

!!! hint
    Využití najdou především u stavových aplikací a clusterovaných aplikací, které ukládají data do nějakého persistentního úložiště. Například MySQL, PostgreSQL, MongoDB, Redis, Kafka, …

## Namespaces

V K8s můžete vytvářet virtuální clustery pomocí namespaces. Názvy zdrojů (Pods, Deployments, Secrets, …) musí být unikátní v rámci jednoho namespace. Pokud se při vytváření zdroje nespecifikuje namespace, tak je zdroj automaticky vytvořen do namespace default.

!!! hint
    Při použití k8s clusteru více uživateli, kteří pracují na různých projektech je vhodné rozdělit k8s cluster pomocí namespaces, aby si uživatelé nemohli omylem měnit deploymenty pod rukama.  
  

> V Kubernetes clusteru se běžně vyskytují 3 namespaces:
> * default  
>   sem jsou umístěny všechny zdroje, kde při jejich vytváření není specifikován namespace
> * kube-public  
>   namespace, který je čitelný všemi uživateli nehledě na jejich oprávnění
> * kube-system  
>   sem jsou umístěny všechny systémové zdroje

Ne všechny zdroje v Kubernetes jsou umístěny v namespaces. Low-level zdroje jako jsou Nodes a PersistentVolumes nejsou umístěny v žádném namespace.

## Secrets

Secrets slouží k uchování, řízení a bezpečné předání hesel, tokenů a ssh klíčů. Secret je zdroj vytvořený v kubernetes, který obsahuje citlivá data. Abychom mohli secret použít, musíme ho připojit k podu. Secret můžeme do podu předat buď jako soubor(y), nebo jako environment variables.

## ConfigMaps

ConfigMaps umožňují oddělení konfigurace od kontejneru, takže kontejner zůstává univerzální. Podobně jako secret je to zdroj vytvořený v kubernetes obsahující data, které můžeme do podu předat buď jako soubor(y), nebo jako environment variables.