# Cvičení Kubernetes

## Než začnete

Pro následující cvičení je potřeba mít nainstalovaný a funkční kubectl a minikube.

### Kubectl

[Návod instalace kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

### Minikube

Pro správné nainstalování minikube je zapotřebí mít v BIOSU povolenou virtualizaci a nainstalovaný hypervisor (VirtualBox)
[Návod instalace minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/#install-minikube)

Po instalaci minikube spustíte pomocí:

```bash
minikube start
```

Pro naše potřeby budeme potřebovat možná trochu víc zdrojů:

```bash
minikube start --cpus 3 --memory 4096
```

## Traefik Ingress

[Stáhněte si](./examples/traefik-ingress.zip)

První co většinou budeme potřebovat v Kubernetes je nastavit routing/load balancing požadavků. Rozbalte si stažený archiv.

### kubectl apply

Příkaz, který podle konfigurace vytvoří, nebo updatuje zdroje (service, ingress, deployment, pod, ...) v Kubernetes. Alternativou k příkazu apply je příkaz create, který narozdíl od apply umí zdroje pouze vytvořit.

```bash
kubectl apply -f /cesta/ke/slozce/traefik-ingress
```

Pokud se příkaz zpracoval správně, měl by být nasazen traefik ingress v namespace kube-system.

### kubectl get namespace

Příkaz, který vypíše seznam namespaců vytvořených v kubernetes.

```bash
kubectl get namespaces
```

Po provedení tohoto příkazu byste měli vidět následující 3 namespacy:

- default
- kube-public
- kube-system

### kubectl get daemonset

Příkaz, který vypíše seznam daemonsetů v kubernetes.

Zkontrolujeme daemonset, který jsme nasadili (traefik-ingress-controller).

```bash
kubectl get daemonset -n kube-system
```

### kubectl get pod

```bash
kubectl get pod -n kube-system
```

### kubectl get service

```bash
kubectl get service -n kube-system
```

### kubectl get clusterrole + clusterrolebindings

```bash
kubectl get clusterrole

kubectl get clusterrolebindings
```

### kubectl get all

Chceme-li se podívat na services, pods, deployments, daemonsets a replicasets najednou, můžeme použít následující příkaz.

```bash
kubectl get all -n kube-system
```

## Wordpress krok za krokem

[Stáhněte si](./examples/wordpress.zip)

Deployment, který budeme vytvářet se skládá z MySQL a Wordpress. Udělejte deployment opět pomocí `kubect apply -f /path/to/folder`.

```yaml
{! ./Kubernetes/examples/wordpress/wordpress-deployment.yaml !}
```

### kubectl describe

Více informací o zdrojích můžete získat pomocí describe. Describe je užitečný při debugování deploymentu, pokud něco nefunguje.

```bash
kubectl describe pod mysql-57c779955b-8wn6f
kubectl describe pods -l app=wordpress
kubectl describe services config-tester
kubectl describe deployments config-tester
```

### kubectl logs

Další informace o běhu podu můžete získat pomocí `kubectl logs podname`.

```bash
kubectl logs mysql-57c779955b-8wn6f
```

### restartování podu

Kubernetes pody nerestartuje, ale stejného efektu můžeme docílit zabitím nodu a jeho následným nastartováním. Protože náš mysql deployment má určeno, že má běžet 1 replica, tak nám bude stačit pod smazat. Kubernetes se pak postará o to, aby běžel právě jeden pod mysql.

```bash
kubectl delete pod mysql-57c779955b-8wn6f

# výsledek si můžeme ověřit:

kubectl get pods
```

!!! warning
    Pokud smažeme deployment, nebo nějaký jiný zdroj, tak už k žádnému restartu nemusí dojít.


## Configs

[Stáhněte si](./examples/configs.zip)

Po stažení opět deployněte do kubernetes.

Ověření, že vše funguje můžete provést pomocí následujících adres ve webovém prohlížeči:

```
# přečte evnironment variables v kontejneru
http://tester.192.168.99.101.xip.io/envs

# přečte obsah složky /secret
http://tester.192.168.99.101.xip.io/readdir?q=/secret

# přečte obsah souboru /secret/password
http://tester.192.168.99.101.xip.io/readfile?q=/secret/password
```

### Service

```yaml
{! ./Kubernetes/examples/configs/service.yaml !}
```

### Ingress

```yaml
{! ./Kubernetes/examples/configs/ingress.yaml !}
```

### Configmap

Ukázka, jak může vypadat configmapa:

```yaml
{! ./Kubernetes/examples/configs/configmap-env.yaml !}
```

Další varianta configmapy:

```yaml
{! ./Kubernetes/examples/configs/configmap-file.yaml !}
```

### Secret

```yaml
{! ./Kubernetes/examples/configs/secret.yaml !}
```

### Deployment

```yaml
{! ./Kubernetes/examples/configs/deployment.yaml !}
```

## Další užitečné příkazy

```bash
# autocompletion pro bash
source <(kubectl completion bash)

# další pomoc pro debugging
kubectl get events

kubectl cluster-info

# naslouchá na lokálním portu 8888 a předává komunikaci do podu na portu 5000 
kubectl port-forward pod/mypod 8888:5000

# spuštění podu pro debugging
kubectl run -ti debug-pod --rm --restart=Never --image=mcr.microsoft.com/dotnet/core/sdk -- bash

# kopírování secretu z jednoho namespace do druhého
kubectl get secret regcred --export -o yaml | kubectl apply --namespace=develop -f -

```
