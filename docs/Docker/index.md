# Co je Docker?

Docker je nástroj pro práci s kontejnery (jejich vytváření, deployment a provoz). Zjednodušuje provozování aplikací na serveru, protože izoluje nastavení jednotlivých aplikací a jejich správu.

Kontejnerizace umožňuje vývojáři "zabalit" aplikaci společně se všemi knihovnami a závislostmi, které aplikace potřebuje ke svému běhu do jednoho image.

Výhoda kontejnerizace oproti provozování virtuálních strojů (VM) spočívá především ve výkonu a managementu. Nevýhodou je, že provozovaný kontejner běží ve stejném kernelu, jako jeho hostovací stroj, takže není možné používat odlišné operační systémy (nejde provozovat windows docker image na linuxovém OS). Z hlediska bezpečnosti to může být také problematické, proto např. není nejlepší používat neověřené image.

Docker
![Docker](./images/docker_architecture.png)

VM
![VM](./images/vm_architecture.png)

## Image

Image je originál (šablona), podle které se vytváří kontejnery. Skládá se ze základního (base) image a vrstev (layers). Image jsou neměnné - zapsáním změny do image se vytvoří nový image.

### Base Image

Base image je image, který nemá žádného předka. Typickým base imagem bývá operační systém se základní sadou nástrojů (apt, yum, …).

### Layers

Každá vrstva image představuje změnu mezi sebou a base imagem.  

!!! hint
    Například:  
    - Base image + nakopírování souboru, nebo složky  
    - Image + přidání environment variable  
    - Image + instalace aplikace

## Container

Container v terminologii dockeru je instancí image.

Následující příkaz stáhne z docker registrů image, který se jmenuje node (node.js) a vytvoří z něj kontejner, který spustí.

`docker run node`

### Dockerfile

Dockerfile je soubor, který obsahuje instrukce pro Docker, jak má sestavit image. Každý příkaz v dockerfile souboru představuje vytvoření nové vrstvy.

```dockerfile
# Definice base image.
FROM dockerfile/ubuntu

# Instalace nginx (přidání vrstvy).
RUN \
  add-apt-repository -y ppa:nginx/stable && \
  apt-get update && \
  apt-get install -y nginx && \
  rm -rf /var/lib/apt/lists/* && \
  echo "\ndaemon off;" >> /etc/nginx/nginx.conf && \
  chown -R www-data:www-data /var/lib/nginx

# Definice pracovní složky
WORKDIR /etc/nginx

# Definice příkazu, který se provede ve workdir po
# spuštění docker containeru vytvořeného z tohoto image
CMD ["nginx"]
```

## Registry

Registry jsou místem, kde jsou uloženy všechny docker image. Je to obdoba něco jako GitHub, ale pro docker image.

Jako registr můžeme použít třeba oficiální - Docker Hub, nebo si spustit vlastní docker registr: `docker run -d -p 5000:5000 --restart always --name registry registry:2`

### Docker Hub

Docker Hub je oficiální služba pro docker registry. Docker si stahuje image defaultně z docker hubu, pokud nenastavíme jiné registry, kam se má docker připojovat.