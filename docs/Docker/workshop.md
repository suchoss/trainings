# Cvičení

Pro následující cvičení je potřeba mít nainstalovaný a funkční docker. V případě přístupu k internetu přes proxy pak i správně nastavenou proxy v dockeru.

!!! tip
    Ověření, že máte nainstalován docker provedete příkazem `docker version`
    Ověření přístupu k docker hubu pak `docker run hello-world`.

## Práce s Dockerem

### Jak vytvořit vlastní image

Vytvořit vlastní image není nic složitého. Stačí znát syntaxi potřebnou k vytvoření Dockerfilu.

#### Dockerfile Ubuntu + NodeJs + hello world

V tomto příkladě vezmeme **ubuntu** base image, přidáme do něj jednu vrstvu nainstalováním **Node.js** a následně nakopírujeme zdrojové soubory naší js **hello world** aplikace. Z výsledného image poté spuštěním vytvoříme kontejner, který bude naslouchat na localhostu, portu 8888.

Dockerfile, který budeme vytvářet:

```dockerfile
{! ./Docker/examples/nodejs/Dockerfile !}
```

1. [Stáhněte si](./examples/nodejs.zip) první příklad a rozbalte ho někam na disk. Ve složce nodejs najdete tři soubory:
    * Package.json  
    Obsahuje informace o závislostech aplikace pro npm.
    * server.js  
    Zdrojové kódy naší aplikace, kterou chceme spustit v Node.js.
    * Dockerfile  
    Tenhle soubor nám říká, jak bude vypadat výsledný image, pokud vše proběhne správně.
2. V příkazové řádce si přepněte pracovní složku do právě rozbaleného adresáře (nodejs).

### Build image

Příkaz, který v aktuálním umístění vyhledá Dockerfile a podle instrukcí v něm vytvoří image a uloží do lokálních registrů. Přepínač **-t** potom image pojmenuje.

```Bash
# syntaxe:
# docker build <cesta k dockerfile>
# nebo
# docker image build <cesta k dockerfile>

docker image build -t skupina/node-boring-app .
```

!!! tip
    Přepínač **-t** se zapisuje ve stylu: '*autor/nazev image:tag*'.  

    V případě nepoužití tagu docker automaticky přiděluje na místo verze *:latest*.  

    Pokud používáte jiné než oficiální docker registry, tak je potřeba před jméno autora ještě přidat url adresu privátních registrů: '*myregistryhost:5000/autor/nazev:tag*'.

### Help

Pro nápovědu, jaké příkazy a nápovědu k jednotlivým příkazům použijte:

```Bash
docker help
docker help run
```

### Create container

Příkaz run spustí image skupina/node-boring-app.  

Přepínač **-p** nastaví port ve tvaru &lt;local port:container port&gt;.  

Přepínač **-d** zajistí, že je image spuštěný na pozadí (neblokuje konzoli).  

Přepínač **-v** nám umožní persistenci dat mimo kontejner. Používá se ve tvaru &lt;local folder|local file:container folder|container file&gt;.

```Bash
# syntaxe:
# docker run <image name>
# nebo
# docker container run <image name>

docker run -d -p 8888:8080 skupina/node-boring-app
```

!!! caution
    Příkaz run se používá k vytvoření a spuštění kontejneru v jednom kroce, ne k jeho nastartování poté, co byl zastaven. K tomu složí příkaz `start`

### Vyhledání spuštěných kontejnerů

```bash
docker ps
# nebo
docker container ls
```

Pokud se podařilo spustit container z předchozího příkladu, tak bychom měli vidět něco podobného po zadání `docker ps`, případně jeho alternativou `docker container ls`:

![docker ps](./images/docker_ps.png)

### Konzolový přístup ke kontejneru

Přepínač **-it** slouží pro interaktivní přístup přes tty, takže se můžeme z naší příkazové řádky připojit přímo do konzole kontejneru.
Přepínač **-d** slouží pro spuštění příkazu na pozadí.

```bash
# syntaxe:
# docker exec <parameter> <container name|container id> <command>
# nebo
# docker container exec <parameter> <container name|container id> <command>

# pro vytvoření souboru execWorks ve složce /tmp/
docker exec -d laughing_mendel touch /tmp/execWorks

# pro interaktivní režím:
docker exec -it laughing_mendel bash
```

### Zastavení kontejneru

```bash
# syntaxe:
# docker stop <container name|container id>
# nebo
# docker container stop <container name|container id>

docker container stop laughing_mendel
```

### Vyhledání všech kontejnerů

Přepínač **-a** způsobí, že se vypíšou všechny existující kontejnery.

```bash
docker ps -a
# nebo
docker container ls -a
```

### Spuštění zastaveného kontejneru

```bash
# syntaxe:
# docker start <container name|container id>
# nebo
# docker container start <container name|container id>

docker container start laughing_mendel
```

### Přístup k logům

```bash
# syntaxe:
# docker logs <container name|container id>
# nebo
# docker container logs <container name|container id>

docker logs laughing_mendel
```

### Získání informací o kontejneru

```bash
# syntaxe
# docker inspect <container name|container id>
# nebo
# docker container inspect <container name|container id>

docker inspect laughing_mendel
```

### Smazání kontejneru

Přepínač **-f** způsobí, že jde smazat i spuštěný kontejner.

```bash
# syntaxe:
# docker rm <container name|container id>
# nebo
# docker container rm <container name|container id>

docker container rm laughing_mendel -f
```

!!! caution
    Smazáním kontejneru nezajistíte smazání image, ze kterého byl kontejner vytvořen.

### Seznam lokálních image

```bash
# Seznam pojmenovaných (otagovaných) image
docker image ls
# Seznam všech image
docker image ls -a
```

### Přehled vrstev/historie image

```bash
# syntaxe
# docker image history <image name|image id>
docker image history skupina/node-boring-app
```

### Smazání lokálního image

```bash
# syntaxe
# docker image rm <image name|image id>
docker image rm skupina/node-boring-app
```

### Smazání všech nepoužívaných image

```bash
docker image prune -a
```

### Přihlášení k jinému docker registry

Pokud nezadáte username pomocí přepínače **-u** a heslo pomocí přepínače **-p**, budete k němu vyzváni.

```bash
docker login myregistryhost:5000
```

### Volumes

Spusťte si na porovnání dva následující příkazy:

```bash
# container bez persistence
docker run -itd ubuntu

# container s persistencí > všechna data zapsaná do /var/dirincontainer se promítnou do /var/dirincomputer a obráceně
docker run -itd -v /var/dirincomputer:/var/dirincontainer ubuntu
```

Otestovat chování si můžete pomocí příkazu `docker exec -it <container_name> bash`.

### Další užitečné příkazy

```bash
# vypíše informace o systému
docker info
# odstraní všechny nepoužívané containery, sítě, image, a svazky.
docker system prune -a --volumes
# vypíše místo na disku obsazené dockerem
docker system df
```
