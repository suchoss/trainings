# Docker
Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and ship it all out as one package.

## Container {#Container}
Docker container is an open source software development platform. Its main benefit is to package applications in “containers,” allowing them to be portable among any system running the Linux operating system (OS).

## Image {#Image}
An image serves as the blueprint for creating containers that will host a certain application or service. Images are designed to be immutable; when you want to update what is inside your containers, you build a new image, rather than update your existing images.

## Registry {#Registry}
A registry is a place where you store container images. It’s like a GitHub repository, except for containers. Registries can be hosted in the cloud or on-premises.

### Docker Hub
Docker Hub is Docker’s official registry service, which runs in the cloud. It’s easy to use, but it’s only one of numerous registry services.
